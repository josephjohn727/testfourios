//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var baba = SKSpriteNode()
    var wall = SKSpriteNode()
    var stopBlock = SKSpriteNode()
    var flagBlock = SKSpriteNode()
    var flag = SKSpriteNode()
    var wallBlock = SKSpriteNode()
    var winBlock = SKSpriteNode()
    var isBlock = SKSpriteNode()
    let babaSpeed:CGFloat = 43
    
    var gameInProgress = true

    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        //Initialise the baba
        self.baba = self.childNode(withName: "baba") as! SKSpriteNode
        self.baba.physicsBody!.categoryBitMask = PhysicsCategory.Baba
        self.baba.physicsBody!.collisionBitMask = PhysicsCategory.Walls | PhysicsCategory.Blocks
        self.baba.physicsBody!.contactTestBitMask = PhysicsCategory.Blocks
      
        
        // Initilaise the wall
        self.wall = self.childNode(withName: "wall") as! SKSpriteNode
        self.wall.physicsBody!.categoryBitMask = PhysicsCategory.Walls
        self.wall.physicsBody!.collisionBitMask = PhysicsCategory.None
        self.wall.physicsBody!.contactTestBitMask = PhysicsCategory.None
        
        
        // Initilise the stopBlock
        self.stopBlock = self.childNode(withName: "stopblock") as! SKSpriteNode
        self.stopBlock.physicsBody!.categoryBitMask = PhysicsCategory.Blocks
        self.stopBlock.physicsBody!.collisionBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks
        self.stopBlock.physicsBody!.contactTestBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks | PhysicsCategory.Walls
        self.stopBlock.physicsBody!.isDynamic = true
        
        // Initilise the flagBlock
        self.flagBlock = self.childNode(withName: "flagblock") as! SKSpriteNode
        self.flagBlock.physicsBody!.categoryBitMask = PhysicsCategory.Blocks
        self.flagBlock.physicsBody!.collisionBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks
        self.flagBlock.physicsBody!.contactTestBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks | PhysicsCategory.Walls
        self.flagBlock.physicsBody!.isDynamic = true
        
        // Initilise the flag
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
        self.flag.physicsBody!.categoryBitMask = PhysicsCategory.Blocks
        self.flag.physicsBody!.collisionBitMask = PhysicsCategory.None
        self.flag.physicsBody!.contactTestBitMask = PhysicsCategory.None
        self.flag.physicsBody!.isDynamic = true
        
        // Initilise the wallBlock
        self.wallBlock = self.childNode(withName: "wallblock") as! SKSpriteNode
        self.wallBlock.physicsBody!.categoryBitMask = PhysicsCategory.Blocks
        self.wallBlock.physicsBody!.collisionBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks
        self.wallBlock.physicsBody!.contactTestBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks | PhysicsCategory.Walls
        self.wallBlock.physicsBody!.isDynamic = true
        
        // Initilise the winBlock
        self.winBlock = self.childNode(withName: "winblock") as! SKSpriteNode
        self.winBlock.physicsBody!.categoryBitMask = PhysicsCategory.Blocks
        self.winBlock.physicsBody!.collisionBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks
        self.winBlock.physicsBody!.contactTestBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks | PhysicsCategory.Walls
        self.winBlock.physicsBody!.isDynamic = true
        
        // Initilise the isBlock
        self.isBlock = self.childNode(withName: "isblock") as! SKSpriteNode
        self.isBlock.physicsBody!.categoryBitMask = PhysicsCategory.Blocks
        self.isBlock.physicsBody!.collisionBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks
        self.isBlock.physicsBody!.contactTestBitMask = PhysicsCategory.Baba | PhysicsCategory.Blocks | PhysicsCategory.Walls
        
        
        func babaWins() {
            self.gameInProgress = false


            let message = SKLabelNode(text:"CONGRAGULATION!")
            message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
            message.fontSize = 100
            message.fontName = "AvenirNext-Bold"
            message.fontColor = UIColor.black
            addChild(message)
            print("you win!")

        }
        
        func gameOver() {
            self.gameInProgress = false
            print("you lose, try again!")
           
        }
        
        
    }
    
    
    
   
    func didBegin(_ contact: SKPhysicsContact) {
        //print("Something collided!")
        // detect the collision to deactivate the stop rules
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if(nodeA == nil || nodeB == nil){
            return
        }
        if(nodeA!.name == "baba" && nodeB!.name == "stopblock") {
            print("baba hit stop")
            
            //        // setup physics for the wall
            //self.wall.physicsBody = SKPhysicsBody(edgeFrom: <#T##CGPoint#>, to: <#T##CGPoint#>)
            
            // self.wall.physicsBody?.categoryBitMask = 1
            self.baba.physicsBody?.collisionBitMask = 0
            //self.wall.physicsBody?.contactTestBitMask = 10
            print("wall unlocked")
            
        }else if(nodeA!.name == "winblock" && nodeB!.name == "isblock"){
            print("win hits isBlock")
            
        }else if(nodeA!.name == "flagblock" && nodeB!.name == "isblock"){
            print("flagBlock hits isBlock")
        }
        
      
    }
    
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
  
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // GET THE POSITION WHERE THE MOUSE WAS CLICKED
        // ---------------------------------------------
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        
        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        print("Player touched: \(nodeTouched)")
        
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "upButton") {
            // move up
            self.baba.position.y = self.baba.position.y + babaSpeed
        }
        else if (nodeTouched == "downButton") {
            // move down
            self.baba.position.y = self.baba.position.y - babaSpeed
        }
        else if (nodeTouched == "leftButton") {
            // move left
            self.baba.position.x = self.baba.position.x - babaSpeed
        }
        else if (nodeTouched == "rightButton") {
            // move right
            self.baba.position.x = self.baba.position.x + babaSpeed
        }
        
    }
    
    struct PhysicsCategory {
        static let None:  UInt32 = 0
        static let Baba:  UInt32 = 0b1      // 0x00000001 = 1
        static let Blocks: UInt32 = 0b10     // 0x00000010 = 2
        static let Walls:  UInt32 = 0b100    // 0x00000100 = 4
       
    }
}
